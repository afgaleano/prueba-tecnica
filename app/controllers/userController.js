const bcrypt = require('bcrypt');
const saltRounds = 10;
const pool = require('../models/db');

async function updateUser(req, res) {
  const { id, pwd, name, lastName, email, areaCode } = req.body;

  // Verificar que se proporcionó el id del usuario a actualizar
  if (!id) {
    return res.status(400).json({ error: 'No se proporcionó el id del usuario a actualizar' });
  }
  
  // verificar que la contraseña cumpla los requisitos de suministrarse para actualizar
  if (pwd && !validateUserPassword(pwd)){
    return res.status(400).json({ error: 'La contraseña suministrada no cumple con los criterios de seguridad' });
  }

  const updateData = {};
  const updateFields = {};

  pwd && validateUserPassword(pwd) ? (updateData.pwd = await bcrypt.hash(pwd, saltRounds), updateFields.pwd = 'pwd') : null;
  name ? (updateData.name = name, updateFields.name = 'name') : null;
  lastName ? (updateData.lastName = lastName, updateFields.lastName = 'last_name') : null;
  email ? (updateData.email = email, updateFields.email = 'email') : null;
  areaCode ? (updateData.areaCode = areaCode, updateFields.areaCode = 'area_code') : null;
  
  const fieldList = Object.values(updateFields).join(' = ?, ') + ' = ?';
  const valueList = Object.keys(updateData).map((key) => updateData[key]);
  // Agregar id al array de valores
  valueList.push(id);

  // Verificar si hay campos para actualizar
  if (valueList.length === 1) {
    return res.status(400).json({ error: 'No se proporcionaron datos para actualizar' });
  }
  // Realziar consulta en la base de datos
  const queryString = `UPDATE user SET ${fieldList} WHERE id = ?`;

  try {
    const results = await pool.query(queryString, valueList);
    return res.status(200).json({
      status: 'success',
      message: `Usuario con id ${id} actualizado correctamente`
    });
  } catch (error) {
    return res.status(500).json({ status: 'error', error: 'Error al actualizar el usuario' });
  }
}

async function getSingleUserData(req, res) {
    const { id } = req.params;
    const sql = 'SELECT user.*, area.name AS area_name FROM user JOIN area ON area.code = user.area_code WHERE id = ?';
    try {
      const results = await pool.query(sql, id);
      res.status(200).json({
        status: 'success',
        data: results
      });
    } catch (error) {
      res.status(500).json({
        status: 'error',
        message: 'Error interno'
      });
    }
}
  
async function getAllUserData(req, res) {
    const sql = 'SELECT user.*, area.name AS area_name FROM user JOIN area ON area.code = user.area_code';
    try {
      const results = await pool.query(sql);
      res.status(200).json({
        status: 'success',
        data: results
      });
    } catch (error) {
      res.status(500).json({
        status: 'error',
        message: 'Error interno'
      });
    }
}
function validateUserPassword(pwd) {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#^-])[A-Za-z\d@$!%*?&^-]{8,}$/;
    return regex.test(pwd);
}
async function generateUserName(name, lastName) {
    let userName = name.charAt(0).toLowerCase() + lastName.toLowerCase();
    // Condicion nombre de usuario unico
    const queryString = 'SELECT COUNT(*) as count FROM user WHERE username REGEXP ?;';
    const usernameRegex = `^${userName}(\\d+[^a-zA-Z]*)?$`;
    
    try {
        const results = await pool.query(queryString, [usernameRegex]);
        count = results[0].count;
        // Agrego contador al nombre de usuario en caso de encontrarse en uso
        if(count){
            userName = userName + count;
        }
        return userName;
    } catch (error) {
        throw new Error('Internal server error');
    }
}

async function createUser(req, res) {
  try {
    const { id, pwd, name, lastName, email, areaCode } = req.body;

    // Verificar si todos los campos requeridos están presentes
    if (!id || !pwd || !name || !lastName || !email || !areaCode) {
      return res.status(400).json({ status: 'error', message: 'Faltan datos' });
    }

    // Verificar si la contraseña cumple con los requisitos
    if (!validateUserPassword(pwd)) {
      return res.status(400).json({ status: 'error', message: 'La contraseña no cumple las condiciones' });
    }

    // Cifrar la contraseña antes de guardarla
    const surePwd = await bcrypt.hash(pwd, saltRounds);

    // Generar el nombre de usuario
    const userName = await generateUserName(name, lastName);

    // Insertar el usuario en la base de datos
    const insertQueryString = 'INSERT INTO user (id, pwd, name, last_name, email, area_code, username) VALUES (?, ?, ?, ?, ?, ?, ?)';
    const insertQueryParams = [id, surePwd, name, lastName, email, areaCode, userName];

    try {
      const results = await pool.query(insertQueryString, insertQueryParams);
      return res.status(200).json({ status: 'success', message: `Usuario ${userName} creado correctamente` });
    } catch (error) {
      return res.status(500).json({ status: 'error', message: 'Error al crear el usuario' });
    }

  } catch (error) {
    return res.status(500).json({ status: 'error', message: 'Error al crear el usuario' });
  }
}
async function deleteUser(req, res) {
  const { id } = req.params;

  if (!id) {
    return res.status(400).json({ status: 'error', message: 'No se proporcionó el id del usuario a eliminar' });
  }

  try {
    const result = await pool.query('DELETE FROM user WHERE id = ?', [id]);
    if (result.affectedRows === 0) {
      return res.status(404).json({ status: 'error', message: `No se encontró un usuario con id ${id}` });
    }
    return res.status(200).json({ status: 'success', message: `Usuario con id ${id} eliminado correctamente` });
  } catch (error) {
    return res.status(500).json({ status: 'error', message: 'Error interno' });
  }
}

module.exports = {
updateUser,
deleteUser,
getSingleUserData,
getAllUserData,
createUser,
};