const mysql = require('mysql');
const { promisify } = require('util');
require('dotenv').config();

const pool = mysql.createPool({
  host: process.env.db_host,
  user: process.env.db_user,
  password: process.env.db_pwd,
  database: process.env.db_name,
  connectionLimit: 10, // Agregar límite de conexiones para evitar errores de conexión
});

pool.getConnection((err, connection) => {
  if (err) {
    switch (err.code) {
      case 'PROTOCOL_CONNECTION_LOST':
        console.error('Conexión con la base de datos perdida');
        break;
      case 'ER_CON_COUNT_ERROR':
        console.error('Demasiadas conexiones a la base de datos');
        break;
      case 'ECONNREFUSED':
        console.error('Conexión a la base de datos rechazada');
        break;
      default:
        console.error('Error de conexión a la base de datos', err.code);
    }
  } else if (connection) {
    connection.release();
    console.log('Conexión con la base de datos exitosa');
  }
});

// Hago posible que las queries sean procesadas
pool.query = promisify(pool.query);

module.exports = pool;
