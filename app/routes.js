const express = require('express');
const multer = require('multer');
const userController = require('./controllers/userController');

const router = express.Router();
const upload = multer();

// Rutas para manipular los usuarios
router.put('/users/:id', upload.any(), userController.updateUser);
router.get('/users', userController.getAllUserData);
router.get('/users/:id', userController.getSingleUserData);
router.post('/users', upload.any(), userController.createUser);
router.delete('/users/:id', userController.deleteUser);

// Ruta 404 para las rutas no encontradas
router.use((req, res) => {
  res.status(404).json({ status: 'error', message: 'Ruta no encontrada' });
});

module.exports = router;