const express = require('express');
const helmet = require('helmet');
const routes = require('./routes');
const bodyParser = require('body-parser');

require('dotenv').config();

const app = express();

// Middleware para añadir seguridad a la aplicación
app.use(helmet());

// Middleware para analizar el cuerpo de la solicitud en formato "multipart/form-data"
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.text());
app.use(express.raw());

// Middleware para analizar el cuerpo de la solicitud en formato JSON y codificado en URL
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configuración de las rutas
app.use('/api', routes);

// Ruta 404
app.use((req, res) => {
  res.status(404).json({ status: 'error', message: 'Ruta no encontrada' });
});

// Obtener el número de puerto desde la variable de entorno
const port = process.env.PORT || 3000;

// Arranque del servidor
app.listen(port, () => {
  console.log(`El servidor está en ejecución en el puerto ${port}.`);
});
